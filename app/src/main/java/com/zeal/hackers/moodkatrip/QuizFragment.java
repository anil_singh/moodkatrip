package com.zeal.hackers.moodkatrip;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.alienapp.findlocation.MainActivity;
import com.alienapp.findlocation.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by anil on 7/2/16.
 */
public class QuizFragment extends Fragment implements WebService.WebServiceListner {

    List<Quiz_> quiz_list = new ArrayList<Quiz_>();
    private EditText questionEditText;
    private EditText choiceAEditText;
    private EditText choiceBEditText;
    private EditText choiceCEditText;
    private EditText choiceDEditText;
    TextView textView;
    private RadioGroup choicesRadioGroup;
    static int counter;
    Button nextButton;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.quiz, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        counter = 0;

        new WebService(getActivity(), this).execute();
        questionEditText = (EditText) getActivity().findViewById(R.id.question_text);
        choiceAEditText = (EditText) getActivity().findViewById(R.id.choice_a_text);
        choiceBEditText = (EditText) getActivity().findViewById(R.id.choice_b_text);
        choiceCEditText = (EditText) getActivity().findViewById(R.id.choice_c_text);
        choiceDEditText = (EditText) getActivity().findViewById(R.id.choice_d_text);
        choicesRadioGroup = (RadioGroup) getActivity().findViewById(R.id.choices_radio_group);
        nextButton = (Button) getActivity().findViewById(R.id.new_quiz_button);
    textView = (TextView) getActivity().findViewById(R.id.mood);

        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nextQuiz();
            }
        });

    }

    public void setQuiz(int quiz_no) {
        if (quiz_no < quiz_list.size()) {
            Quiz_ quiz = quiz_list.get(quiz_no);
            String quiz_name = quiz.getQuiz();
            Options options = quiz.getOptions();
            questionEditText.setText(quiz_name);
            if (options.get1() != null) {
                choiceAEditText.setText(options.get1());
            } else {
                choiceAEditText.setText("Not Applicable");
            }

            if (options.get2() != null) {
                choiceBEditText.setText(options.get2());
            } else {
                choiceBEditText.setText("Not Applicable");
            }

            if (options.get3() != null) {
                choiceCEditText.setText(options.get3());
            } else {
                choiceCEditText.setText("Not Applicable");
            }

            if (options.get4() != null) {
                choiceDEditText.setText(options.get4());
            } else {
                choiceDEditText.setText("Not Applicable");
            }

        }
    }

    public void nextQuiz() {
        counter++;
        if (counter > quiz_list.size())
        {
            textView.setText("Your mood is happy");
//            MainActivity.fragmentManager.beginTransaction().remove(MainActivity.fragmentManager.findFragmentByTag("Quiz")).commit();
//            MainActivity.fragmentManager.beginTransaction().add(R.id.map_layout, MainActivity.fragmentManager.findFragmentByTag("Recommend")).commit();
        }else {

            setQuiz(counter);
        }
    }

    @Override
    public void onRequestComplete(List<Quiz_> quizData) {

        Log.d("Quiz", "onRequestComplete() returned: " + quizData.size());
        quiz_list = quizData;
        setQuiz(counter);
    }
}
