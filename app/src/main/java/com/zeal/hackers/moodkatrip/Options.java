
package com.zeal.hackers.moodkatrip;

import java.util.HashMap;
import java.util.Map;

public class Options {

    private String _1;
    private String _2;
    private String _3;
    private String _4;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * 
     * @return
     *     The _1
     */
    public String get1() {
        return _1;
    }

    /**
     * 
     * @param _1
     *     The 1
     */
    public void set1(String _1) {
        this._1 = _1;
    }

    /**
     * 
     * @return
     *     The _2
     */
    public String get2() {
        return _2;
    }

    /**
     * 
     * @param _2
     *     The 2
     */
    public void set2(String _2) {
        this._2 = _2;
    }

    /**
     * 
     * @return
     *     The _3
     */
    public String get3() {
        return _3;
    }

    /**
     * 
     * @param _3
     *     The 3
     */
    public void set3(String _3) {
        this._3 = _3;
    }

    /**
     * 
     * @return
     *     The _4
     */
    public String get4() {
        return _4;
    }

    /**
     * 
     * @param _4
     *     The 4
     */
    public void set4(String _4) {
        this._4 = _4;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
