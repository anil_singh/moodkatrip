
package com.zeal.hackers.moodkatrip;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Quiz {

    private List<Quiz_> quiz = new ArrayList<Quiz_>();
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * 
     * @return
     *     The quiz
     */
    public List<Quiz_> getQuiz() {
        return quiz;
    }

    /**
     * 
     * @param quiz
     *     The quiz
     */
    public void setQuiz(List<Quiz_> quiz) {
        this.quiz = quiz;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
