package com.zeal.hackers.moodkatrip;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.alienapp.findlocation.R;

/**
 * Created by anil on 7/2/16.
 */
public class Recommendation extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.recomendation, container, false);
        return view;
    }
}
