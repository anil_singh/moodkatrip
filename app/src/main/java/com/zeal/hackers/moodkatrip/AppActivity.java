/****************************************************************************
Copyright (c) 2008-2010 Ricardo Quesada
Copyright (c) 2010-2012 cocos2d-x.org
Copyright (c) 2011      Zynga Inc.
Copyright (c) 2013-2014 Chukong Technologies Inc.

http://www.cocos2d-x.org

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
 ****************************************************************************/
package com.zeal.hackers.moodkatrip;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import com.alienapp.findlocation.MainActivity;
import com.alienapp.findlocation.R;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphRequest.GraphJSONArrayCallback;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.share.ShareApi;
import com.facebook.share.Sharer;
import com.facebook.share.Sharer.Result;
import com.facebook.share.model.GameRequestContent;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;
import com.facebook.share.widget.GameRequestDialog;
import com.facebook.share.widget.ShareDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;

public class AppActivity
{

	Activity instance;
	public CallbackManager callbackManager;
	GameRequestDialog requestDialog;
	SharedPreferences preferences;
    CallBackAfterLogin callBackAfterLogin;
	String userId = "";
	//	public native void 

	
	public AppActivity(MainActivity activity)
	{
		instance = activity;
        callBackAfterLogin = activity;
		preferences = instance.getPreferences(instance.MODE_PRIVATE);
		FacebookSdk.sdkInitialize(instance);
		
		Log.d("create","Hello anilSucess");
		callbackManager = CallbackManager.Factory.create();

		requestDialog = new GameRequestDialog(instance);
		requestDialog.registerCallback(callbackManager, gameRequestCallback);
		/*Uri targetUrl = AppLinks.getTargetUrlFromInboundIntent(this, getIntent());
		if (targetUrl != null)
		{
			Log.i("Activity", "App Link Target URL: " + targetUrl.toString());
		}*/

		LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>(){

			@Override
			public void onSuccess(LoginResult result) 
			{
				// TODO Auto-generated method stub
				Log.d("LoginManager", "On Success " + result.toString());
				getUserInfo();
			}

			@Override
			public void onError(FacebookException error) {
				// TODO Auto-generated method stub
				Log.d("LoginManager", "On Error");
			}

			@Override
			public void onCancel() {
				// TODO Auto-generated method stub
				Log.d("LoginManager", "On Cancel");
			}
		});
	}
	
	//02-06 23:38:47.559: D/Garph else(26897):   "id": "1029703363719362",


	/*public static AppActivity getIntance()
	{
		Log.d("FaceBook", "Showing alert dialog: ");
		return instance;
	}*/

	public void fbLogin()
	{
		if(AccessToken.getCurrentAccessToken()==null)
			LoginManager.getInstance().logInWithReadPermissions(instance, Arrays.asList("public_profile","user_friends","email"));
	}

	

	public void fbFriendsId()
	{
		if (AccessToken.getCurrentAccessToken()!=null)
		{
			GraphRequest.newMyFriendsRequest(AccessToken.getCurrentAccessToken(), new GraphJSONArrayCallback()
			{

				@Override
				public void onCompleted(JSONArray objects, GraphResponse response) {
					// TODO Auto-generated method stub
					try 
					{
						Log.d("Garph Friend ", objects.toString(2));
						
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}).executeAndWait();;
		}
	}



	public void publishPhoto()
	{
		if (hasPublishPermission())
		{

			Bitmap image = BitmapFactory.decodeResource(instance.getResources(), R.mipmap.ic_launcher);
			SharePhoto sharePhoto = new SharePhoto.Builder().setBitmap(image).build();
			ArrayList<SharePhoto> photos = new ArrayList<>();
			photos.add(sharePhoto);

			SharePhotoContent sharePhotoContent = new SharePhotoContent.Builder().setPhotos(photos).build();
			ShareApi.share(sharePhotoContent,shareCallback);
		}
		else
		{
			LoginManager.getInstance().logInWithPublishPermissions(instance, Arrays.asList("publish_actions"));
		}
	}

	private boolean hasPublishPermission()
	{
		AccessToken accessToken = AccessToken.getCurrentAccessToken();

		return accessToken != null && accessToken.getPermissions().contains("publish_actions");
	}

	private void postStatusUpdate()
	{
		if(hasPublishPermission())
		{
			Log.d("PostStatus", "Ist");
			Profile profile = Profile.getCurrentProfile();
			ShareLinkContent linkContent = new ShareLinkContent.Builder()
			.setContentTitle("Hello Facebook")
			.setContentDescription(	"The 'Hello Facebook' sample  showcases simple Facebook integration")
			.setContentUrl(Uri.parse("http://developers.facebook.com/docs/android"))
			.build();
			if(ShareDialog.canShow(ShareLinkContent.class))
			{
				Log.d("PostStatus", "2nd");
				ShareDialog shareDialog = new ShareDialog(instance);
				shareDialog.registerCallback(callbackManager, shareCallback);

			}
			else if (profile != null) 
			{
				Log.d("PostStatus", "3rdt");
				ShareApi.share(linkContent, shareCallback);
			} 
		}
		else
		{
			LoginManager.getInstance().logInWithPublishPermissions(instance, Arrays.asList("publish_actions"));
		}

	}

	void appInvite()
	{
		/*String appLinkUrl, previewImageUrl;

		appLinkUrl = "https://fb.me/483225705174617";
		previewImageUrl = "https://lh3.googleusercontent.com/Knx0vKEkZh0uva6OKTDcvePbMAr0BkekKkETHIBEB12fT8ZMd2jJcu1ZZaPCBJMdpNA2=w300-rw";
		Log.d("In appinvie", "Invite "+AppInviteDialog.canShow());
		if (AppInviteDialog.canShow())
		{
			Log.d("In appinvie", "Invite");
			AppInviteContent content = new AppInviteContent.Builder()
			.setApplinkUrl(appLinkUrl)
			.setPreviewImageUrl(previewImageUrl)
			.build();

			AppInviteDialog.show(this, content);
		}*/

		GameRequestContent content = new GameRequestContent.Builder()
		.setMessage("Come play this level with me")
		.build();
		requestDialog.show(content);

		/*GameRequestContent content = new GameRequestContent.Builder()
		.setMessage("Come play this level with me")
		.setActionType(ActionType.SEND)
		.setObjectId("YOUR_OBJECT_ID")
		.build();
		requestDialog.show(content);*/

	}

	public void getUserInfo()
	{
		if(AccessToken.getCurrentAccessToken()==null)
			LoginManager.getInstance().logInWithReadPermissions(instance, Arrays.asList("public_profile","user_friends","email"));
		else
		{
			GraphRequest.newMeRequest(AccessToken.getCurrentAccessToken(), new GraphRequest.GraphJSONObjectCallback() 
			{
				@Override
				public void onCompleted(JSONObject user, GraphResponse graphResponse) {

					try 
					{
						Log.d("User Id", "" + user.getString("id").toString());
						Log.d("Garph else", user.toString(2));
						String userId = user.getString("id");
						String name = user.getString("name");
						Editor editor = preferences.edit();
						editor.putString("USER_ID", userId);
						editor.putString("USER_NAME", name);
                        editor.putBoolean("USER_Login", true);
                        editor.commit();
						editor.commit();
						
						
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}).executeAsync();
		}
	}

    public boolean isLogin() {
        return preferences.getBoolean("USER_Login", false);
    }

    public void logOut(){
        LoginManager.getInstance().logOut();
        Editor editor = preferences.edit();
        editor.putBoolean("USER_Login", false);
        editor.commit();
    }
	
	public Bitmap getUserPic() {
		URL imageURL;
		userId = preferences.getString("USER_ID", " ");
		Bitmap bitmap = null;
		try {
			imageURL = new URL("https://graph.facebook.com/"
					+ userId + "/picture?type=large");
			bitmap = BitmapFactory.decodeStream(imageURL.openConnection()
					.getInputStream());
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return bitmap;
	}

	private FacebookCallback<GameRequestDialog.Result> gameRequestCallback = new FacebookCallback<GameRequestDialog.Result>()
			{

		@Override
		public void onSuccess(GameRequestDialog.Result result)
		{
			// TODO Auto-generated method stub
			String id = result.getRequestId();
            callBackAfterLogin.loginResult(true);
			Log.d("GRID", "Your id = "+id);
		}

		@Override
		public void onCancel() {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onError(FacebookException error) {
			// TODO Auto-generated method stub
			
		}
	};

	private FacebookCallback<Sharer.Result> shareCallback = new FacebookCallback<Sharer.Result>()
			{
		
		@Override
		public void onSuccess(Result result) {
			// TODO Auto-generated method stub
			
		}
		
		@Override
		public void onError(FacebookException error) {
			// TODO Auto-generated method stub
			
		}
		
		@Override
		public void onCancel() {
			// TODO Auto-generated method stub
			
		}

	};

	public interface CallBackAfterLogin {
		public void loginResult(boolean b);
	}

}
