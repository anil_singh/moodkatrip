package com.zeal.hackers.moodkatrip;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class WebService extends AsyncTask<String, Void, List<Quiz_>> {
    public static final String TAG = "WebService";
    WebServiceListner listner;
	ProgressDialog mProgressDialog;
	Context context;
	double latitude;
	double longitude;

	public WebService(Context activity, QuizFragment quizFragment) {
		context = activity;
		listner = quizFragment;

		mProgressDialog = new ProgressDialog(activity);
		mProgressDialog.setMessage(" Loading... ");
		mProgressDialog.setIndeterminate(false);
		mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		mProgressDialog.setCancelable(false);
	}

	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		if (mProgressDialog != null)
			mProgressDialog.show();
	}

	@Override
	protected List<Quiz_>doInBackground(String... params) {;
		Quiz_ quiz_Data = new Quiz_();
		HttpURLConnection connection = null;
        List<Quiz_> quiz_list = new ArrayList<Quiz_>();
		try {
			URL url = new URL("http://www.json-generator.com/api/json/get/clTxcgTVAi");
			connection = (HttpURLConnection) url.openConnection();
			StringBuffer buffer = new StringBuffer(1024);
			BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			String temp = "";
			while ((temp = reader.readLine()) != null) {
				buffer.append(temp).append('\n');
			}
			reader.close();
			JSONObject data = new JSONObject(buffer.toString());
            JSONArray jsonArray = data.getJSONArray("quiz");

            for (int i = 0; i < jsonArray.length(); i++) {
                Options options = new Options();
                Quiz_ quiz_ = new Quiz_();
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                JSONObject jsonOption = jsonObject.getJSONObject("options");
                String quiz = jsonObject.getString("quiz");
                if (jsonOption.has("1")) {
                    options.set1(jsonOption.getString("1"));
                } else {
                    options.set1(null);
                }

                if (jsonOption.has("2")) {
                    options.set2(jsonOption.getString("2"));
                } else {
                    options.set2(null);
                }

                if (jsonOption.has("3")) {
                    options.set3(jsonOption.getString("3"));
                } else {
                    options.set3(null);
                }

                if (jsonOption.has("4")) {
                    options.set4(jsonOption.getString("4"));
                } else {
                    options.set4(null);
                }

                quiz_.setOptions(options);
                quiz_.setQuiz(quiz);
                quiz_list.add(quiz_);

            }
            Log.d(TAG, "doInBackground() returned: " + quiz_list.size());

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (connection != null)
				connection.disconnect();
		}
		return quiz_list;
	}

	@Override
	protected void onPostExecute(List<Quiz_> result) {
		if (mProgressDialog != null && mProgressDialog.isShowing())
			mProgressDialog.cancel();

		listner.onRequestComplete(result);
	}

	public interface WebServiceListner {
		public void onRequestComplete(List<Quiz_> quizData);
	}

}
