
package com.zeal.hackers.moodkatrip;

import java.util.HashMap;
import java.util.Map;

public class Quiz_ {

    private String quiz;
    private Options options;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * 
     * @return
     *     The quiz
     */
    public String getQuiz() {
        return quiz;
    }

    /**
     * 
     * @param quiz
     *     The quiz
     */
    public void setQuiz(String quiz) {
        this.quiz = quiz;
    }

    /**
     * 
     * @return
     *     The options
     */
    public Options getOptions() {
        return options;
    }

    /**
     * 
     * @param options
     *     The options
     */
    public void setOptions(Options options) {
        this.options = options;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
