package com.alienapp.findlocation;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.alienapp.findlocation.utils.GPSTracker;
import com.zeal.hackers.moodkatrip.AppActivity;
import com.zeal.hackers.moodkatrip.QuizFragment;
import com.zeal.hackers.moodkatrip.Recommendation;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, AppActivity.CallBackAfterLogin {

    public static final int REQUEST_PLACE_PICKER = 1;
    public static final String TAG = "MainActivity";
    FragmentTransaction fragmentTransaction;
    ImageView userImageView;
    TextView userTextView;
    ImageButton fbButton;
    AppActivity appActivity;
    private Recommendation recomFragment;
    private QuizFragment quizFragment;
    public static FragmentManager fragmentManager;
    private MapFunction mapFragment;
    private GPSTracker gpsTracker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);

//        userImageView = (ImageView) navigationView.getHeaderView().findViewById(R.id.userImg);
//        userTextView = (TextView) navigationView.getHeaderView().findViewById(R.id.user_name);


        fbButton = (ImageButton) findViewById(R.id.fb_login);
        recomFragment = new Recommendation();
        appActivity = new AppActivity(this);
        fragmentManager = getSupportFragmentManager();

        if (appActivity.isLogin()) {
            fbButton.setVisibility(View.GONE);
            new GetUserImg().execute();
            fragmentManager.beginTransaction().add(R.id.map_layout, recomFragment, "Recommend").commit();
        }
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        gpsTracker = new GPSTracker(this);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mapFragment.onPickButtonClick();
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();


        navigationView.setNavigationItemSelectedListener(this);


        mapFragment = new MapFunction();
        quizFragment = new QuizFragment();
        mapFragment.setLatitude(gpsTracker.getLatitude());
        mapFragment.setLongitude(gpsTracker.getLongitude());

        fbButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                appActivity.fbLogin();
            }
        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camara) {
            // Handle the camera action
            if (quizFragment.isAdded()) {
                fragmentManager.beginTransaction().remove(quizFragment).commit();
            }
            if (mapFragment.isAdded()) {
                fragmentManager.beginTransaction().remove(mapFragment).commit();
            }
            if (!recomFragment.isAdded()) {
                fragmentManager.beginTransaction().add(R.id.map_layout, recomFragment, "Recommend").commit();
            }
        } else {
            if (id == R.id.nav_gallery) {
                if (quizFragment.isAdded()) {
                    fragmentManager.beginTransaction().remove(quizFragment).commit();
                }
                if (recomFragment.isAdded()) {
                    fragmentManager.beginTransaction().remove(recomFragment).commit();
                }
                if (!mapFragment.isAdded()) {
                    fragmentManager.beginTransaction().add(R.id.map_layout, mapFragment, "Map").commit();
                }

            } else if (id == R.id.nav_slideshow) {
                if (mapFragment.isAdded()) {
                    fragmentManager.beginTransaction().remove(mapFragment).commit();
                }
                if (recomFragment.isAdded()) {
                    fragmentManager.beginTransaction().remove(recomFragment).commit();
                }
                if (!quizFragment.isAdded()) {
                    fragmentManager.beginTransaction().add(R.id.map_layout, quizFragment, "Quiz").commit();
                }

            } else if (id == R.id.nav_manage) {


            }/* else if (id == R.id.nav_share) {

            } else if (id == R.id.nav_send) {

            }*/
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    @Override
    public void loginResult(boolean b) {

        Toast.makeText(this, "Succesfull Login", Toast.LENGTH_SHORT).show();
        if (b) {
            fbButton.setVisibility(View.GONE);
            new GetUserImg().execute();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        appActivity.callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    class GetUserImg extends AsyncTask<Void, Void, Bitmap> {

        @Override
        protected Bitmap doInBackground(Void... params) {
            // TODO Auto-generated method stub
            appActivity.getUserInfo();
            Bitmap bitmap = appActivity.getUserPic();
            return bitmap;
        }

        @Override
        protected void onPostExecute(Bitmap result) {
            // TODO Auto-generated method stub
            Log.d("Image", "Size = " + result.getHeight());
            if (result != null) {
//                userImageView.setImageBitmap(result);
            }
//            SharedPreferences preferences = getPreferences(MainActivity.this.MODE_PRIVATE);

//            String name = preferences.getString("USER_NAME", "Android");
//            userTextView.setText(name);
        }
    }

}
